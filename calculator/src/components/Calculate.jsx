import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { ListGroup } from "react-bootstrap";

let res;
export default class Calculate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: []
    };
  }
  increaseList = () => {
    let patt = /[^0-9]/gi;
    let val_1 = document.getElementById("value_1").value;
    let val_2 = document.getElementById("value_2").value;
    let oper = document.getElementById("Operator").value;
    
    // console.log(this.state.result)
    if(val_1 === "" && val_2 === ""){
      alert("Please input both value, cannot empty!"); 
    }else if(val_1 === "") {
      alert("Please input value 1, cannot empty!");
    } else if(val_2 ===""){
      alert("Please input value 2, cannot empty!");
    }else if (val_1.match(patt) != null & val_2.match(patt) != null) {
      alert("Both value are invalid!");
    }else if(val_1.match(patt) != null){
      alert("Invalid value 1!")
    }else if(val_2.match(patt) != null){
      alert("Invalid value 2!")
    }else {
      if (oper === "+") {
        res = parseInt(val_1) + parseInt(val_2);
        console.log(res);
      }
      if (oper === "-") {
        res = parseInt(val_1) - parseInt(val_2);
        console.log(res);
      }
      if (oper === "*") {
        res = parseInt(val_1) * parseInt(val_2);
        console.log(res);
      }
      if (oper === "/") {
        res = parseInt(val_1) / parseInt(val_2);
        console.log(res);
      }
      if (oper === "%") {
        res = parseInt(val_1) % parseInt(val_2);
        console.log(res);
      }
    }
  };
  render() {
    let re = this.state.result.map((res) => (
      <ListGroup.Item>{res}</ListGroup.Item>
    ));
    return (
      <div className="container-fluid">
        <div className="container">
          <div className="item-left">
            <img src="Calculator.png" alt="Calculator" />
            <Form>
              <Form.Group>
                <Form.Control
                  name="val_1"
                  type="text"
                  value={this.state.val_1}
                  id="value_1"
                />
              </Form.Group>
              <Form.Group>
                <Form.Control name="val_2" type="text" id="value_2" />
              </Form.Group>
              <Form.Group>
                <Form.Control as="select" id="Operator">
                  <option value="+">+ Plus</option>
                  <option value="-">- Subtract</option>
                  <option value="*">* Multiply</option>
                  <option value="/">/ Divide</option>
                  <option value="%">% Module</option>
                </Form.Control>
              </Form.Group>
              <Button variant="primary" onClick={this.increaseList}>
                Calculate
              </Button>{" "}
            </Form>
          </div>
          <div className="item-right">
            <h2>Result History</h2>
            <div className="container">
              <ListGroup className="groupList">{re}</ListGroup>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
