import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Calculate from './components/Calculate';


function App() {
  return (
    <Calculate />
  );
}

export default App;
